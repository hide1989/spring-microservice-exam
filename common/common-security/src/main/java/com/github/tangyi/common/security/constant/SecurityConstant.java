package com.github.tangyi.common.security.constant;

/**
 * @author tangyi
 * @date 2018-08-25 14:08
 */
public class SecurityConstant {

    /**
     * 基础角色
     */
    public static final String BASE_ROLE = "role_user";

    /**
     * 超级管理员角色
     */
    public static final String ROLE_ADMIN = "role_admin";

    /**
     * 默认生成图形验证码过期时间
     */
    public static final int DEFAULT_IMAGE_EXPIRE = 60;

    /**
     * 正常状态
     */
    public static final String NORMAL = "0";

    /**
     * 异常状态
     */
    public static final String ABNORMAL = "1";
}
